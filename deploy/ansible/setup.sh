#!/usr/bin/env bash
set -e
# add tools to ansible-controller
ansible-playbook control.yaml

echo 'Server Name, IP Address' > servers.csv


# Create and configure Servers
ansible-playbook jenkins_create_servers.yaml --vault-id .vault-password.txt
ansible-playbook jenkins_manage.yaml -i lab-jenkins.hosts --vault-id .vault-password.txt

ansible-playbook elasticsearch_create.yaml --vault-id .vault-password.txt
ansible-playbook elasticsearch_manage.yaml -i lab-elasticsearch.hosts  --vault-id .vault-password.txt


# Create app servers
ansible-playbook database_create_servers.yaml --vault-id .vault-password.txt
ansible-playbook webapp_create_servers.yaml --vault-id .vault-password.txt

# Configure Servers
ansible-playbook database_manage.yaml -i lab-db.hosts --vault-id .vault-password.txt
ansible-playbook webapp_manage.yaml -i lab-webapp.hosts --vault-id .vault-password.txt