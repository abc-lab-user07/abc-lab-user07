#!/usr/bin/env bash
set -e

rm -f ~/.kube/config

az aks create -g selenium-group -n selenium-grid --node-count 1 --generate-ssh-keys
az aks get-credentials -g selenium-group -n selenium-grid
kubectl run selenium-grid --image selenium/hub:3.3.0 --port 4444
kubectl expose deployment selenium-grid --type=LoadBalancer --name=selenium-grid
kubectl get service selenium-grid
kubectl run selenium-node-chrome --image selenium/node-chrome:3.3.1 --env="HUB_PORT_4444_TCP_ADDR=selenium-grid" --env="HUB_PORT_4444_TCP_PORT=4444"
kubectl scale deployment selenium-node-chrome --replicas=3
