package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.ArrayList;

import com.abc.workshop.cicd.drugstore.dto.AddressDTO;
import com.abc.workshop.cicd.drugstore.service.AddressService;
import com.abc.workshop.cicd.drugstore.utils.TestDataHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AddressControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AddressService addressService;

    @Test
    public void testGetAllAddresses() throws Exception {

        Mockito.when(this.addressService.getAllAddresses()).thenReturn(new ArrayList<AddressDTO>() {
            {
                add(TestDataHelper.getAddress(1));
                add(TestDataHelper.getAddress(2));
                add(TestDataHelper.getAddress(3));
            }
        });

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].addressType", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].addressLine1", Is.is("Test address first line 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].addressLine2", Is.is("Test address second line 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].city", Is.is("Test city 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].state", Is.is("TX")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].postalCode", Is.is("75001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].customerId", Is.is(1)))

                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Is.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].addressType", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].addressLine1", Is.is("Test address first line 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].addressLine2", Is.is("Test address second line 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].city", Is.is("Test city 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].state", Is.is("TX")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].postalCode", Is.is("75001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].customerId", Is.is(2)))

                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", Is.is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].addressType", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].addressLine1", Is.is("Test address first line 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].addressLine2", Is.is("Test address second line 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].city", Is.is("Test city 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].state", Is.is("TX")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].postalCode", Is.is("75001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].customerId", Is.is(3)))
                .andReturn();

    }

    @Test
    public void testGetOneAddress() throws Exception {

        Mockito.when(this.addressService.getAddress(1L)).thenReturn(TestDataHelper.getAddress(1));

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/addresses/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressType", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressLine1", Is.is("Test address first line 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressLine2", Is.is("Test address second line 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.city", Is.is("Test city 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.state", Is.is("TX")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.postalCode", Is.is("75001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId", Is.is(1)))
                .andReturn();
    }

    @Test
    public void testUpdateAddress() throws Exception {

        AddressDTO address = TestDataHelper.getAddress(1);
        address.setAddressLine1("A new test address line");
        Mockito.when(this.addressService.saveAddress(Mockito.any())).thenReturn(address);

        String addressJson = new ObjectMapper().writeValueAsString(address);

        MockHttpServletRequestBuilder request =
                MockMvcRequestBuilders.put("/api/v1/addresses/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(addressJson);

        MvcResult result = this.mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressType", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressLine1", Is.is("A new test address line")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addressLine2", Is.is("Test address second line 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.city", Is.is("Test city 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.state", Is.is("TX")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.postalCode", Is.is("75001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId", Is.is(1)))
                .andReturn();

    }

}
