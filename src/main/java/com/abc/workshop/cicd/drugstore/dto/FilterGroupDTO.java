package com.abc.workshop.cicd.drugstore.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FilterGroupDTO {
    @JsonProperty("GroupOp")
    private Integer groupOp;
    @JsonProperty("Conditions")
    private List<FilterConditionDTO> conditions;

    public Integer getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(Integer groupOp) {
        this.groupOp = groupOp;
    }

    public List<FilterConditionDTO> getConditions() {
        return conditions;
    }

    public void setConditions(List<FilterConditionDTO> conditions) {
        this.conditions = conditions;
    }
}
