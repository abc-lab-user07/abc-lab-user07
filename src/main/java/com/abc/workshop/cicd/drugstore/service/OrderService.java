package com.abc.workshop.cicd.drugstore.service;

import java.util.List;

import com.abc.workshop.cicd.drugstore.dto.OrderDTO;

/**
 * Service interface for performing order-related operations.
 */
public interface OrderService {
    /**
     * Returns the list of all orders for a given customer.
     *
     * @param customerId The customer ID
     * @return The list of all order objects
     */
    List<OrderDTO> getAllOrders(Long customerId);

    /**
     * Returns the order object for the specified order ID if found.
     *
     * @param orderId The order ID
     * @return The order object
     */
    OrderDTO getOrder(Long orderId);

    /**
     * Saves the order info.
     *
     * @param request The order information
     * @return the updated order object
     */
    OrderDTO saveOrder(OrderDTO request);

    /**
     * Deletes an existing order.
     *
     * @param orderId The ID of the order to delete
     */
    void deleteOrder(Long orderId);
}
