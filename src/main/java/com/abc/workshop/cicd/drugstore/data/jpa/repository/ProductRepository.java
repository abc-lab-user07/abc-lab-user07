package com.abc.workshop.cicd.drugstore.data.jpa.repository;

import java.util.List;

import com.abc.workshop.cicd.drugstore.data.jpa.entity.ProductEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring data JPA repository used to access product information from the database.
 */
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    Page<ProductEntity> findByProductCategoryProductCategoryIdIn(List<Long> productCategoryIds, Pageable pageable);
}
