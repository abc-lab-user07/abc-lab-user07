package com.abc.workshop.cicd.drugstore.dao;

import java.util.List;

import com.abc.workshop.cicd.drugstore.model.Customer;

/**
 * Data Access Object for performing customer-related operations.
 */
public interface CustomerDAO {
    /**
     * Returns the list of all customer objects.
     *
     * @return the list of all customer objects
     */
    List<Customer> getAllCustomers();

    /**
     * Returns the customer object for the specified customer ID, if found.
     *
     * @param customerId the customer ID
     * @return the customer object
     */
    Customer getCustomer(Long customerId);

    /**
     * Saves customer info.
     *
     * @param customer the customer information
     * @return the updated customer object
     */
    Customer saveCustomer(Customer customer);

    /**
     * Deletes an existing customer.
     *
     * @param customerId the ID of the customer to delete
     */
    void deleteCustomer(Long customerId);
}
