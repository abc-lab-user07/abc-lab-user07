package com.abc.workshop.cicd.drugstore.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.CustomerDAO;
import com.abc.workshop.cicd.drugstore.dto.CustomerDTO;
import com.abc.workshop.cicd.drugstore.model.Customer;
import com.abc.workshop.cicd.drugstore.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
    private static String CUSTOMER_NOT_FOUND_MESSAGE = "Customer not found for id: ";

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private MapperUtils mapper;

    public List<CustomerDTO> getAllCustomers() {
        List<Customer> customers = customerDAO.getAllCustomers();

        return customers.stream().map(customer -> mapper.map(customer, CustomerDTO.class)).collect(Collectors.toList());
    }

    public CustomerDTO getCustomer(Long customerId) {
        Customer customer = customerDAO.getCustomer(customerId);
        if (customer == null) {
            throw new ResourceNotFoundException(CUSTOMER_NOT_FOUND_MESSAGE + customerId);
        }

        return this.mapper.map(customer, CustomerDTO.class);
    }

    public CustomerDTO saveCustomer(CustomerDTO request) {
        Customer customer = mapper.map(request, Customer.class);
        this.validateCustomerExists(request);

        customerDAO.saveCustomer(customer);

        return mapper.map(customer, CustomerDTO.class);
    }

    public void deleteCustomer(Long customerId) {
        customerDAO.deleteCustomer(customerId);
    }

    private void validateCustomerExists(CustomerDTO request) {
        if (request.getCustomerId() != null) {
            CustomerDTO existingCustomer = this.getCustomer(request.getCustomerId());
            if (existingCustomer == null) {
                throw new ResourceNotFoundException(CUSTOMER_NOT_FOUND_MESSAGE + request.getCustomerId());
            }
        }
    }
}
