package com.abc.common.exception;


public class InternalServerErrorException extends RuntimeException {
    private static final long serialVersionUID = -1888192473417873058L;

    public InternalServerErrorException() {}

    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InternalServerErrorException(String message) {
        super(message);
    }

    public InternalServerErrorException(Throwable cause) {
        super(cause);
    }
}
