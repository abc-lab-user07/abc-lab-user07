# CI/CD Lab

### Prerequisites:

* Bitbucket Account - bitbucket.org
* Git command line - https://git-scm.com/downloads
* Java 8 or better - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


Optional:

* Source Tree - https://www.sourcetreeapp.com/






### Running application

```
gradlew.bat clean bootRun
```


URL: [http://localhost:8080/](http://localhost:8080/)



### Running unit tests:


```
gradlew.bat clean test -x integrationTest
```

